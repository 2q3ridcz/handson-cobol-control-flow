# handson-cobol-control-flow

Play around with COBOL Control Flow, a VSCode extension.

## Usage

1. Git clone this repository and open in VSCode.
2. VSCode popup appears and recommends extensions.
3. Install those extensions.
4. Add your cobol source code file to the repository.
5. Open the source code file in VSCode, right click and select "Generate COBOL Control Flow".
6. In a preview tab, a flowchart shows up.

## Reference

- [COBOL Control Flow - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=broadcomMFD.ccf)
    - A VSCode extension. Draws flowchart of cobol source code file in a preview tab.
- [GitHub - uwol/proleap-cobol-parser: ProLeap ANTLR4-based parser for COBOL](https://github.com/uwol/proleap-cobol-parser)
    - Contains lots of sample cobol files in /src/test/resources/
